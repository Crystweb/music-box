package ua.danit.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import ua.danit.model.User;

public interface UserDao extends JpaRepository<User, Integer> {


}
