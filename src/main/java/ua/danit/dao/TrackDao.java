package ua.danit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import ua.danit.model.Track;


public interface TrackDao extends JpaRepository<Track,Integer> {
}
