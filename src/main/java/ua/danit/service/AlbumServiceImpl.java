package ua.danit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.danit.dao.AlbumDao;
import ua.danit.model.Album;


@Service
public class AlbumServiceImpl implements AlbumService {

  @Autowired
  private AlbumDao albumDao;

  @Override
  public Album addAlbum(Album album) {
    return albumDao.save(album);
  }

  @Override
  public Iterable<Album> getAllAlbum() {
    return albumDao.findAll();
  }

  @Override
  public Album getAlbumById(int id) {
    return albumDao.findOne(id);
  }

  @Override
  public void update(int id, Album album) {
    Album newAlbum = albumDao.findOne(id);
    newAlbum.setTitle(album.getTitle());
    newAlbum.setCover(album.getCover());
    newAlbum.setTracks(album.getTracks());
    albumDao.save(newAlbum);
  }

  @Override
  public void delete(int id) {
    albumDao.delete(id);
  }

  @Override
  public void deleteCover(int id) {
    Album newAlbum = albumDao.findOne(id);
    newAlbum.setCover(null);
    albumDao.save(newAlbum);

  }
}
