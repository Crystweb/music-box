package ua.danit.service;


import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.danit.dao.AlbumDao;
import ua.danit.dao.TrackDao;
import ua.danit.model.Album;
import ua.danit.model.Track;
import ua.danit.model.User;


@Service
public class TrackServiceImpl implements TrackService {

  @Autowired
  private TrackDao tracksDao;
  @Autowired
  private AlbumDao albumDao;
  @Autowired
  private UserService userService;

  @Override
  public void addTrack(int id, Track track) {
    Album al = albumDao.findOne(id);
    al.getTracks().add(track);
    albumDao.save(al);
    tracksDao.save(track);
  }

  @Override
  public Iterable<Track> getAllTracks() {
    return tracksDao.findAll();
  }

  @Override
  public Iterable<Track> getLikedTracks(int id) {
     return userService.getUserById(id).getTracks();
  }

  @Override
  public Track getTrackById(int id) {
    return tracksDao.findOne(id);
  }

  @Override
  public void update(Track track) {
    tracksDao.save(track);
  }

  @Override
  public void delete(int id) {
    tracksDao.delete(id);
  }

  @Override
  public void addToFavorites(int id, Track track) {
    User user= userService.getUserById(id);
    user.getTracks().add(track);
    userService.update(user);
  }


}
