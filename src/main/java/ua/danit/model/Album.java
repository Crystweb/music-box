package ua.danit.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity

public class Album {
  @Id
  @GeneratedValue

  private int id;

  private String title;
  private String cover;

  @OneToMany(cascade = CascadeType.ALL)
  @JoinColumn(name = "album_id")
  private List<Track> tracks ;

  public Album() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getCover() {
    return cover;
  }

  public void setCover(String cover) {
    this.cover = cover;
  }

  public List<Track> getTracks() {
    return tracks;
  }

  public void setTracks(List<Track> tracks) {
    this.tracks = tracks;
  }

  @Override
  public String toString() {
    return "Album{" +
        "id=" + id +
        ", title='" + title + '\'' +
        ", cover='" + cover + '\'' +
        ", tracks=" + tracks +
        '}';
  }
}
