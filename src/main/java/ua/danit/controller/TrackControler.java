package ua.danit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ua.danit.model.Track;
import ua.danit.model.User;
import ua.danit.service.TrackService;


@RestController
@RequestMapping("/track")
public class TrackControler {
  @Autowired
  private TrackService trackService;


  @RequestMapping(path = "/tracks/{id}", method = RequestMethod.POST)
  void addTrack(@PathVariable int id, @RequestBody Track track) {
    trackService.addTrack(id, track);
  }

  @RequestMapping (value = "/tracks/{id}/like",method = RequestMethod.POST)
  void addLike(@PathVariable int id,@RequestBody Track trackId){
    trackService.addToFavorites(id,trackId);
  }

  @RequestMapping(value = "/tracks/{id}/like",method = RequestMethod.GET)
  @ResponseBody
  Iterable<Track> getTrackFavoriteByUserID (@PathVariable int id){
    return trackService.getLikedTracks(id);
  }

}
