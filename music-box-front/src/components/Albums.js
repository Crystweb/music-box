import React from 'react';
import OneAlbum from './OneAlbum';
import {connect} from 'react-redux'

class Albums extends React.Component{

  render(){
    return (
      <div className="albums">
        {this.props.albums.map( post =>
          <OneAlbum
            key={post.id}
            post={post}
          />)}
      </div>
  )
  }
}
export default connect(state => ({
    albums: state.albums
  }),
  dispatch =>({
   onLoad:(albumName)=>{
      dispatch({type:'ALBUMS_LOADED',payload:albumName});
    }

  }))(Albums);
