import React from 'react';
import {Link} from 'react-router-dom';
import AlbumsTracks from './AlbumsTracks'

class OneAlbum extends React.Component{
  render(){
    let post = {id:'', title:'',cover:''};
    post = this.props.post !== null ? this.props.post : post;
    return (
      <Link to={"/albums/"+post.id+"/tracks"} post={post}>
       <div className="albumsCanvas"
         key={post.id}>
           <img className="albumsCanvas_img"
             src={post.cover}
             alt="img"
           />
           <h1 className="albumsCanvas_title">
             {post.title}
           </h1>
       </div>
     </Link>
     )
   }
}
export default OneAlbum;
