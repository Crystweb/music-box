import React from 'react';
import Albums from './Albums';
import Favorites from './Favorites'
import Admin from './Admin'
import LogIn from './LogIn'
import AlbumsTracks from './AlbumsTracks'
import {Route,Link,Switch} from 'react-router-dom';
import {connect} from 'react-redux'

class App extends React.Component{
  componentDidMount () {
    if(this.props.albums.length==0){
      fetch('http://localhost:8080/albums')
      .then(response => response.json())
      .then(albums => this.props.onLoad(albums))
    }
}


    render(){

        return (
            <div>
                <header>
                    <Link to="/">Albums</Link>
                    <Link to="/favorites">Favorites</Link>
                    <Link to="/admin">Admin</Link>
                    <Link to="/login">Log In</Link>

                </header>
                <Switch>
                    <Route exact path="/" component={Albums}/>
                    <Route exact path="/albums/:id/tracks" component={AlbumsTracks}/>
                    <Route exact path="/favorites" component={Favorites}/>
                    <Route exact path="/admin" component={Admin}/>
                    <Route exact path="/login" component={LogIn}/>

                </Switch>
            </div>
        )
    }
}
const  mapDispatchToProps = dispatch=> {
  return{
    onLoad:albums=>{
      dispatch({type:'ALBUMS_LOADED',payload:albums});
    }
  }
}
const mapStateToProps = state =>{
    return {
      albums: state.albums
    }
}

export default  connect(mapStateToProps, mapDispatchToProps)(App);
