import React,{Component} from 'react';

export default class Gallery extends Component {

    render() {
      const tracks = this.props.tracks;

        return (

            <div className="gallery">
              {this.props.tracks.map((track,index)=><p key={index}>{track.trackName}</p>)}
            </div>
        );
    }
}
