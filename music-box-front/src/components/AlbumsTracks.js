import React,{Component} from 'react';
import {connect} from 'react-redux'
import Gallery from './Gallery'
import OneAlbum from './OneAlbum'

 class AlbumsTracks extends Component {

    render() {
      const id = this.props.match.params.id
      const album = this.props.albums.find(album => album.id==id)
      this.props.onLoadTrack(album.track);
        return (<div>
            <OneAlbum post={album}/>
            <Gallery  tracks={this.props.tracks} />
          </div>
        );
    }
}
  const  mapDispatchToProps = dispatch=> {
    return{
      onLoadTrack:track=>{
        dispatch({type:'TRACKS_LOADED',payload:track});
      }
    }
  }
  const mapStateToProps = state =>{
    return{
      albums: state.albums,
      tracks: state.tracks
    }
  }
export default connect(mapStateToProps,mapDispatchToProps)(AlbumsTracks)
