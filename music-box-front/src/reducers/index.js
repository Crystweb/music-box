import { combineReducers } from 'redux'
import albums from './albums'
import tracks from './tracks'


export default combineReducers({
    albums,
    tracks
});
